﻿using CrossMedia.Models;
using CrossMedia.Models.Database;
using CrossMedia.Models.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CrossMedia.Controllers
{
    [Authorize]
    public class BudgetController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            var model = new BudgetViewModel();
            var currentLoggedInUser = User.Identity.GetUserId();

            // Al deze linq queries worden nog niet uitgevoerd..
            // Pas in de "index.cshtml" worden ze uitgevoerd.
            //alle items van de ingelogde gebruiker ophalen

            //groupeer op Category, bijv: als een item er meerdere keren in voorkomt dat deze opgeteld word
            //en verdeeld worden per categorie
            var result = from item in db.ItemsPerUser
                where item.UserID.Equals(currentLoggedInUser)
                group item by item.Item.CategoryID
                into groep
                select new TotalOrderItems
                {
                    ItemId = groep.Key,
                    Totaal = groep.Sum(x => x.Quantity.Value * x.Item.Price)
                };
            model.TotalOrderCosts = result.ToArray();

            ViewBag.Expenses = result;

            //De budgets per categorie van de gebruiker 
            var budgets =
                db.Budgets.Where(b =>
                    b.UserID.Equals(currentLoggedInUser, StringComparison.InvariantCultureIgnoreCase));

            model.Budgets = budgets.ToArray();

            // je moet nu je model vullen met de informatie die je nog hebt op je pagina.
            // En je html pagina maken van het type BudgetViewModel  , Alles zit in BudgetViewModel toch wat ik gebruik
            return View(model);
        }
    }
}