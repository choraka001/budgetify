﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CrossMedia.Startup))]
namespace CrossMedia
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
