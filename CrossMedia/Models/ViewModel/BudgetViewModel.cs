﻿using CrossMedia.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrossMedia.Models.ViewModel
{
    public class BudgetViewModel
    {
        public IEnumerable<Budget> Budgets { get; set; }
        public IEnumerable<Category> Category { get; set; }
        public IEnumerable<TotalOrderItems> TotalOrderCosts { get; set; }
    }

    public class TotalOrderItems
    {
        public int ItemId { get; set; }
        public double Totaal { get; set; }
    }
}