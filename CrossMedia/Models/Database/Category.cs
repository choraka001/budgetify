﻿using System.Collections.Generic;

namespace CrossMedia.Models.Database
{
    public class Category
    {
        public int CategoryID { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}