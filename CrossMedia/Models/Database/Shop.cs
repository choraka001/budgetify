﻿namespace CrossMedia.Models.Database
{
    public class Shop
    {
        public int ShopID { get; set; }
        public string Name { get; set; }

        public virtual Location Location { get; set; }
    }
}