﻿using System.Collections.Generic;

namespace CrossMedia.Models.Database
{
    public class Location
    {
        public int LocationID { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }

        public virtual ICollection<Shop> Shops { get; set; }
    }
}