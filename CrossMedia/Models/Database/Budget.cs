﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrossMedia.Models.Database
{
    public class Budget
    {
        public int Id { get; set; }
        public string UserID { get; set; }
        public int CategoryID { get; set; }
        public DateTime Date { get; set; }

        public double Value { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Category Category { get; set; }
    }
}