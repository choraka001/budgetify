﻿namespace CrossMedia.Models.Database
{
    public class Review
    {
        public int ReviewID { get; set; }
        public string UserID { get; set; }
        public int? ShopID { get; set; }
        public int? ItemID { get; set; }
        public string Description { get; set; }
        public bool Like { get; set; }

        public virtual ApplicationUser User { get; set; }
    }
}