﻿namespace CrossMedia.Models.Database
{
    public class Item
    {
        public int ItemID { get; set; }
        public int CategoryID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public double? Barcode { get; set; }

        public virtual Category Category { get; set; }
    }
}