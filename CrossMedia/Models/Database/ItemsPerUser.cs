﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CrossMedia.Models.Database
{
    public class ItemsPerUser
    {
        public int Id { get; set; }
        public int ItemID { get; set; }
        public string UserID { get; set; }
        public int ShopID { get; set; }
        public DateTime PurchasedOn { get; set; }
        public bool? Recurring { get; set; }
        public int? Quantity { get; set; }

        public virtual ApplicationUser User { get; set; }
        public virtual Item Item { get; set; }
        public virtual Shop Shop { get; set; }
    }
}