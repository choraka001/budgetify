namespace CrossMedia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbtoegevoegd : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ItemsPerUsers", newName: "ItemsPerUser");
            RenameTable(name: "dbo.Items", newName: "Item");
            RenameTable(name: "dbo.Categories", newName: "Category");
            RenameTable(name: "dbo.Shops", newName: "Shop");
            RenameTable(name: "dbo.Locations", newName: "Location");
            RenameTable(name: "dbo.Reviews", newName: "Review");
            CreateTable(
                "dbo.Budget",
                c => new
                    {
                        UserID = c.String(nullable: false, maxLength: 128),
                        CategoryID = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Value = c.Double(),
                    })
                .PrimaryKey(t => new { t.UserID, t.CategoryID, t.Date })
                .ForeignKey("dbo.Category", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID, cascadeDelete: true)
                .Index(t => t.UserID)
                .Index(t => t.CategoryID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Budget", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Budget", "CategoryID", "dbo.Category");
            DropIndex("dbo.Budget", new[] { "CategoryID" });
            DropIndex("dbo.Budget", new[] { "UserID" });
            DropTable("dbo.Budget");
            RenameTable(name: "dbo.Review", newName: "Reviews");
            RenameTable(name: "dbo.Location", newName: "Locations");
            RenameTable(name: "dbo.Shop", newName: "Shops");
            RenameTable(name: "dbo.Category", newName: "Categories");
            RenameTable(name: "dbo.Item", newName: "Items");
            RenameTable(name: "dbo.ItemsPerUser", newName: "ItemsPerUsers");
        }
    }
}
