namespace CrossMedia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class KeysAddedBudgetItemsPerUser : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Budget", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.ItemsPerUser", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Budget", new[] { "UserID" });
            DropIndex("dbo.ItemsPerUser", new[] { "UserID" });
            DropPrimaryKey("dbo.Budget");
            DropPrimaryKey("dbo.ItemsPerUser");
            AddColumn("dbo.Budget", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.ItemsPerUser", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Budget", "UserID", c => c.String(maxLength: 128));
            AlterColumn("dbo.ItemsPerUser", "UserID", c => c.String(maxLength: 128));
            AddPrimaryKey("dbo.Budget", "Id");
            AddPrimaryKey("dbo.ItemsPerUser", "Id");
            CreateIndex("dbo.Budget", "UserID");
            CreateIndex("dbo.ItemsPerUser", "UserID");
            AddForeignKey("dbo.Budget", "UserID", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.ItemsPerUser", "UserID", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ItemsPerUser", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Budget", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.ItemsPerUser", new[] { "UserID" });
            DropIndex("dbo.Budget", new[] { "UserID" });
            DropPrimaryKey("dbo.ItemsPerUser");
            DropPrimaryKey("dbo.Budget");
            AlterColumn("dbo.ItemsPerUser", "UserID", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Budget", "UserID", c => c.String(nullable: false, maxLength: 128));
            DropColumn("dbo.ItemsPerUser", "Id");
            DropColumn("dbo.Budget", "Id");
            AddPrimaryKey("dbo.ItemsPerUser", new[] { "ItemID", "UserID", "ShopID" });
            AddPrimaryKey("dbo.Budget", new[] { "UserID", "CategoryID", "Date" });
            CreateIndex("dbo.ItemsPerUser", "UserID");
            CreateIndex("dbo.Budget", "UserID");
            AddForeignKey("dbo.ItemsPerUser", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Budget", "UserID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
        }
    }
}
